package org.binar.controller;

import org.binar.config.JwtUtils;
import org.binar.model.Role;
import org.binar.model.UserDetailsImpl;
import org.binar.model.Users;
import org.binar.model.enumeration.ERoles;
import org.binar.model.request.ForgotPasswordRequest;
import org.binar.model.request.SignupRequest;
import org.binar.model.response.JwtResponse;
import org.binar.model.response.MessageResponse;
import org.binar.repository.RoleRepository;
import org.binar.repository.UserRepository;
import org.binar.service.OtpGenerator;
import org.binar.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/auth")
@CrossOrigin("*")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository usersRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    private OAuth2AuthorizedClientService authorizedClientService;

    @Autowired
    UserDetailsServiceImpl userService;

    @Autowired
    OtpGenerator otpGenerator;

    public AuthController() {

    }

    public AuthController(AuthenticationManager authenticationManager, UserRepository usersRepository,
                           JwtUtils jwtUtils, RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
        this.authenticationManager = authenticationManager;
        this.usersRepository = usersRepository;
        this.jwtUtils = jwtUtils;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping("/signin")
    public ResponseEntity<JwtResponse> authenticateUser(@Valid @RequestBody Map<String, Object> login) {
        // authenticate user dengan username dan password yang diberikan
        // authentication akan dilakukan dengan meng-hash password-nya dengan password yg ada di database (sudah di hash juga)
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(login.get("username"), login.get("password"))
        );

        System.out.println("auth lewat");
        // set object authentication untuk context security
        SecurityContextHolder.getContext().setAuthentication(authentication);

        // generate token dari object authentication yang sudah didapat
        String jwt = jwtUtils.generateJwtToken(authentication);

        // ambil roles yang dimiliki oleh user yg bersangkutan
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        // kasih token JWT ke user
        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(), userDetails.getUsername(), userDetails.getEmail(),
                roles));
    }

    @PostMapping("/signup")
    public ResponseEntity<MessageResponse> registerUser(@Valid @RequestBody SignupRequest signupRequest) {
        // check duplicating username
        Boolean usernameExist = usersRepository.existsByUsername(signupRequest.getUsername());
        if(Boolean.TRUE.equals(usernameExist)) {
            return ResponseEntity.badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        // check duplicating emails
        Boolean emailExist = usersRepository.existsByEmail(signupRequest.getEmail());
        if(Boolean.TRUE.equals(emailExist)) {
            return ResponseEntity.badRequest()
                    .body(new MessageResponse("Error: Email is already taken!"));
        }

        // buat object user nya untuk user yg sedang mendaftar
        Users users = new Users(signupRequest.getUsername(), signupRequest.getEmail(),
                passwordEncoder.encode(signupRequest.getPassword()));

        // check roles yg diinput oleh registering user
        Set<String> strRoles = signupRequest.getRole();
        Set<Role> roles = new HashSet<>();

        // jika roles yg ada itu kosong, akan secara otomatis diarahkan menjadi CUSTOMER
        if(strRoles == null) {
            Role role = roleRepository.findByName(ERoles.CUSTOMER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found"));
            roles.add(role);
        } else {
            strRoles.forEach(role -> {
                Role roles1 = roleRepository.findByName(ERoles.valueOf(role))
                        .orElseThrow(() -> new RuntimeException("Error: Role " + role + " is not found"));
                roles.add(roles1);
            });
        }
        users.setRoles(roles);
        usersRepository.save(users);
        return ResponseEntity.ok(new MessageResponse("User registered successfully"));
    }

    @GetMapping("/loginSuccess")
    public ResponseEntity getLoginInfoOA2(OAuth2AuthenticationToken token) {
        OAuth2AuthorizedClient client = authorizedClientService
                .loadAuthorizedClient(
                        token.getAuthorizedClientRegistrationId(),
                        token.getName()
                );

        String userInfoEndpointUri = client.getClientRegistration()
                .getProviderDetails()
                .getUserInfoEndpoint()
                .getUri();
        Map<String, Object> response = new HashMap<>();
        if(!StringUtils.isEmpty(userInfoEndpointUri)) {
            response.put("name", token.getName());
            response.put("token", "Bearer " + client.getAccessToken());
            return new ResponseEntity(response, HttpStatus.OK);
        }
        response.put("status", "ERROR");
        response.put("message", "OAuth2 token is empty");
        return new ResponseEntity(response, HttpStatus.FORBIDDEN);
    }

    // Shoutout to Digdaya Rajab! Terima kasih Rajab
    @PostMapping("/forgot-password")
    public ResponseEntity forgotPassword(@RequestBody ForgotPasswordRequest request) {
        // validasi user melalui OTP beserta email yg diinput
        Users user = userService.validateEmailOtp(request.getEmail(), request.getOtp());
        // if validated -> jalanin modul reset password
        // jika user tidak null
        if(!Objects.isNull(user)) {
            userService.updatePassword(user, passwordEncoder.encode(request.getNewPassword()));
            // email user tentang aktifitas pergantian password
            System.out.println("password untuk " + user.getUsername() + " telah berhasil diganti");
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        // if not validated -> kasih error message berupa OTP salah
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/otp")
    public ResponseEntity getOtp(@RequestParam("email") String email) {
        // check if email exist
        if(userService.emailExist(email)) {
            // if user ada -> generate OTP
            String otp = otpGenerator.generateOtp(email);
            // OTP yg generated dikirim ke email ybs
            System.out.println(otp);
        }
        return new ResponseEntity(HttpStatus.OK);
    }
}

