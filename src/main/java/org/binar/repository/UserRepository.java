package org.binar.repository;

import org.binar.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<Users, Long> {
    Optional<Users> findByUsername(String username);
    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);

    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "update users set otp = :otp where email = :email")
    void updateOtp(@Param("otp") String otp, @Param("email") String email);

    @Query(nativeQuery = true, value = "select u from users u where email = :email and otp = :otp")
    Users findUsersByEmailAndOtp(@Param("email") String email, @Param("otp") String otp);

    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "update users set password = :newPassword where email = :email")
    void updatePassword(@Param("newPassword") String newPassword, @Param("email") String email);
}
