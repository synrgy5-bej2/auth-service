package org.binar.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class OtpGenerator {

    @Autowired
    UserDetailsServiceImpl userService;

    public String generateOtp(String email) {
        // generate otp, dengan library Random
        Random rand = new Random();
        int number = rand.nextInt(999999);
        String otp = String.format("%06d", number);
        // user dengan email yg diinput akan diupdate entity nya dengan set otp
        userService.updateOtp(otp, email);
        // return otp yg udah di generate
        return otp;
    }
}
