package org.binar.service;

import org.binar.model.UserDetailsImpl;
import org.binar.model.Users;
import org.binar.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    UserRepository usersRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users user = usersRepository.findByUsername(username).orElseGet(null);
        // do something
        return UserDetailsImpl.build(user);
    }

    // @Author : Bestyvincen
    public boolean emailExist(String email) {
        return usersRepository.existsByEmail(email);
    }

    public void updateOtp(String otp, String email) {
        usersRepository.updateOtp(otp, email);
    }

    public Users validateEmailOtp(String email, String otp) {
        return usersRepository.findUsersByEmailAndOtp(email, otp);
    }

    public void updatePassword(Users users, String newPassword) {
        usersRepository.updatePassword(newPassword, users.getEmail());
    }

}
