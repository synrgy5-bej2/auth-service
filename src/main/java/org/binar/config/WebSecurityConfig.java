package org.binar.config;

import org.binar.model.enumeration.ERoles;
import org.binar.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Configuration
@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    AuthEntryPointJwt unauthorizedHandler;

    @Bean
    public AuthTokenFilter authenticationJwtTokenFilter() {
        return new AuthTokenFilter();
    }

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception{
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                // config object cors yang akan digunakan
                .cors()
                .and()
                // config CSRF untuk FE
                .csrf().disable()
                // handling ketika ada yang tidak authorized / authenticated
                .exceptionHandling().authenticationEntryPoint(this.unauthorizedHandler).and()
                // untuk memastikan 1 user yg login, itu cuma 1 entity yg login
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                // mengatur akses API untuk user, untuk role tertentu
                .authorizeRequests(uri ->
                        // permitAll memperbolehkan semua user untuk mengakses, meskipun belum login
                        uri.antMatchers("/", "/api/auth/**", "/error", "/webjars/**", "/user", "/loginSuccess").permitAll()
                        // hasAuthority memastikan hanya authority tersebut yang bisa akses API yang dicantumkan
                        .antMatchers("/first-controller/**", "/customer/**").hasAuthority(ERoles.CUSTOMER.name())
                        // hasAnyAuthority memberikan akses untuk API ke beberapa roles sekaligus.
                        .antMatchers("/second-controller/**").hasAnyAuthority(ERoles.ADMIN.name(), ERoles.SELLER.name())
                        .anyRequest().authenticated())
                // memberikan logic ketika logout dilakukan user
                .logout(l -> l.logoutSuccessUrl("/"))
                // config agar bisa menggunakan oauth2
                .oauth2Login()
                // config ketika login menggunakan oauth2 sukses
                .successHandler(new AuthenticationSuccessHandler() {
                    @Override
                    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
                        SecurityContextHolder.getContext().setAuthentication(authentication);
                    }
                });
//        http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
    }

}
